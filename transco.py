#!/usr/bin/env python3

import os
import sys
import shutil
import pathlib
import argparse
import subprocess
import concurrent.futures
from typing import Any, Tuple, Generator

CONVERTABLE = (".opus", ".ogg", ".flac", ".wav", ".m4a", ".mp3")

args: argparse.Namespace


def log(*args: Any, **kwargs: Any) -> None:
    kwargs["file"] = sys.stderr
    print(*args, **kwargs)


def normalize_ext(s: str) -> str:
    return "." * (s[0] != ".") + s.casefold()


def mkdir(p: str) -> None:
    if os.path.isdir(p):
        return

    log(f"mkdir {p}")

    if args.dryrun:
        return

    pathlib.Path(p).mkdir(parents=True, exist_ok=True)


def list_tree(p: str) -> Generator[Tuple[str, bool], None, None]:
    for f in os.scandir(p):
        if f.is_dir():
            yield (f.path, True)
            yield from list_tree(f.path)
            continue

        yield (f.path, False)


def convert_file(source_path: str, target_path: str, bitrate: int) -> None:
    cmd = [
        "ffmpeg",
        "-nostdin",
        "-y",
        "-i",
        source_path,
        "-b:a",
        f"{bitrate}k",
        target_path,
    ]

    log(f"ffmpeg {source_path} -> {target_path}")

    if args.dryrun:
        return

    result = subprocess.run(cmd, stderr=subprocess.PIPE)

    if result.returncode != 0:
        log(
            f"\033[41m  Error: {source_path} -> {target_path}\033[0m\n",
            result.stderr.decode(),
        )


def copy_file(source_path: str, target_path: str) -> None:
    log(f"cp {source_path} {target_path}")

    if args.dryrun:
        return

    shutil.copy2(source_path, target_path, follow_symlinks=True)


def convert_recursively(
    source_dir: str,
    target_dir: str,
    target_ext: str,
    bitrate: int,
    force_transcode: bool,
    overwrite_files: bool,
    copy_plain: bool,
) -> None:
    source_dir = os.path.relpath(source_dir)
    target_dir = os.path.relpath(target_dir)

    target_ext = normalize_ext(target_ext)

    mkdir(target_dir)

    with concurrent.futures.ProcessPoolExecutor() as executor:
        for source_path, is_dir in list_tree(source_dir):
            target_path = target_dir + source_path[len(source_dir) :]

            if is_dir:
                mkdir(target_path)
                continue

            target_name, ext = os.path.splitext(target_path)
            ext = ext.casefold()

            media_file = ext in CONVERTABLE
            if media_file:
                target_path = target_name + target_ext

            # Continue, if can't write
            if os.path.isfile(target_path) and not overwrite_files:
                continue

            should_transcode = media_file and (
                force_transcode or ext != target_ext
            )

            if should_transcode:
                executor.submit(convert_file, source_path, target_path, bitrate)
            elif media_file or copy_plain:
                copy_file(source_path, target_path)

        executor.shutdown(wait=True, cancel_futures=False)


def main() -> None:
    arg_parser = argparse.ArgumentParser(
        description="Recursively copy a directory and convert certain media to a chosen format. "
        f"Only {', '.join(CONVERTABLE[:-1])}, and {CONVERTABLE[-1]} files will be transcoded, "
        "everything else will be copied as-is."
    )

    arg_parser.add_argument(
        "source_dir",
        metavar="SOURCE_DIR",
        type=str,
        help="Source directory",
    )
    arg_parser.add_argument(
        "target_dir",
        metavar="DEST_DIR",
        type=str,
        help="Target directory",
    )
    arg_parser.add_argument(
        "-b",
        "--bitrate",
        metavar="N",
        default=320,
        type=int,
        help="Bitrate in kbit/s. Default: 320",
    )
    arg_parser.add_argument(
        "-e",
        "--extension",
        metavar=".EXT",
        default=".mp3",
        type=str,
        help=f"Extension ({'|'.join(CONVERTABLE)}). Default: .mp3",
    )
    arg_parser.add_argument(
        "-t",
        "--transcode",
        action="store_true",
        help="Transcode files, even if target format matches the original",
    )
    arg_parser.add_argument(
        "-d",
        "--dryrun",
        action="store_true",
        help="Dry run",
    )
    arg_parser.add_argument(
        "-f",
        "--force",
        action="store_true",
        help="Overwrite files at destination, if any exist.",
    )
    arg_parser.add_argument(
        "-s",
        "--skip_plain",
        action="store_true",
        help="Skip non-media files (e.g. images), instead of copying them",
    )

    global args
    args = arg_parser.parse_args()

    args.extension = normalize_ext(args.extension)

    if not os.path.isdir(args.source_dir):
        sys.exit(f"{args.source_dir} is not a directory")

    if args.extension not in CONVERTABLE:
        sys.exit(f"Unknown extension {args.extension}")

    if args.dryrun:
        print("=== DRY RUN! ===")
    print(f"{args.source_dir} -> {args.target_dir}")
    print(f"Target format: {args.extension}")
    print(f"Bitrate: {args.bitrate}k")
    print(f"Re-encoding: {args.transcode}")
    print()

    convert_recursively(
        args.source_dir,
        args.target_dir,
        args.extension,
        args.bitrate,
        args.transcode,
        args.force,
        not args.skip_plain,
    )


if __name__ == "__main__":
    main()
